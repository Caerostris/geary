/* Copyright 2011-2014 Yorba Foundation
 *
 * This software is licensed under the GNU Lesser General Public License
 * (version 2.1 or later).  See the COPYING file in this distribution.
 */

public class Geary.PGP {
	private static PGP instance = null;
	private string pgp_begin = "-----BEGIN PGP MESSAGE-----";
	public string password { private get; set; default = ""; }

	private PGP() {
		// Fix for Ubuntu: gpg keeps on using the agent, even with a password supplied
		Environment.unset_variable("GPG_AGENT_INFO");
	}

	public static PGP getInstance() {
		if(instance == null) {
			instance = new PGP();
		}
		return instance;
	}

	public Geary.Email? decrypt_message(Geary.Email email, string? tmp_password = null) {
		// attemp decryption

		string message = email.body.buffer.to_string().chomp();
        if(message.substring(0, 2) == "\r\n") {
            message = message.substring(2, -1);
        }

		string temporary_filename = "";
        try {
            int temporary_handle = FileUtils.open_tmp("geary-message-XXXXXX.txt",
                out temporary_filename);
            FileUtils.set_contents(temporary_filename, message.replace("=3D", "="));
            FileUtils.close(temporary_handle);
        } catch (Error e) {
            debug("Error writing file for PGP decryption");
            return null;
        }

        string gpg_password = password;
        if(tmp_password != null)
        {
        	gpg_password = tmp_password;
        }

		string gpg_stdout = "";
        try {
            string[] spawn_args = { "gpg", "--passphrase", gpg_password,
                "--decrypt", temporary_filename };
            Process.spawn_sync("/",
                spawn_args,
                Environ.get(),
                SpawnFlags.SEARCH_PATH,
                null,
                out gpg_stdout);

            if(gpg_stdout != "")  {
            	Geary.Memory.Buffer message_buffer = new Geary.Memory.StringBuffer(gpg_stdout);
             	email.set_message_body(new Geary.RFC822.Text(message_buffer));
               	return email;
            } else {
            	return null;
            }
		} catch (Error e) {
            debug("GPG not found");
            return null;
        }
	}

	public bool is_pgp(Geary.Email email) {
		string message = email.body.buffer.to_string().chomp();
        if(message.substring(0, 2) == "\r\n") {
            message = message.substring(2, -1);
        }

        if(message.substring(0, pgp_begin.length) == pgp_begin) {
        	return true;
        } else {
        	return false;
        }
	}
}